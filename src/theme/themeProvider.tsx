import React, { ReactNode } from 'react';
import { createTheme, ThemeProvider, styled as s1 } from '@mui/material/styles';
import { createTheme as cr, styled as s2 } from '@mui/material';


interface IProps {
  children: ReactNode,
}

export const AppThemeProvider = ({ children }: IProps) => {
  const theme = createTheme({
    palette: {
      primary: {
        main: '#133b5e',
      }
    },
  });
  return (
    <ThemeProvider theme={theme}>
      {children}
    </ThemeProvider>
  );
};
