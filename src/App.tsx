import React from 'react';
import { css, styled, useTheme } from '@mui/material/styles';

import { AppThemeProvider } from './theme/themeProvider';
import { Stack, Typography } from '@mui/material';
import { TemplateString } from './components/TemplateString';
import { TemplateObject } from './components/TemplateObject';
import { ChipWayOne } from './components/WayOne';
import { ChipWayTwo } from './components/WayTwo';
import { ChipWayThree } from './components/WayThree';


const StyledBackground = styled('div')`
  background-color: #111222;
  height: 100vh;
`;
const StyledTitle = styled(Typography)`
 color: white;
`;


const App = () => {
  const theme = useTheme();

  return (
    <AppThemeProvider>
      <StyledBackground>
        <Stack direction="column" justifyContent="center" alignItems="center" spacing={3}>
          <StyledTitle variant="h5">MUI v5 emotion</StyledTitle>
          {/*<TemplateString />*/}
          <TemplateObject />
          <ChipWayOne text="Chip Way One" color={theme.palette.common.white} backgroundColor="red" />
          <ChipWayTwo
            // css={css({
            //   color: 'white', // проигнорит стили, проброшенные через пропсы
            // })}
            sx={{ fontSize: theme => theme.spacing(3), color: 'white' }}
            text="Chip Way Two"
            // color="red"
            backgroundColor="darkBlue"
          />
          <ChipWayThree text="Chip Way Three" />
        </Stack>
      </StyledBackground>
    </AppThemeProvider>
  )
};

export default App;
