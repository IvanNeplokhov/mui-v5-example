import { SxProps } from '@mui/material';
import { Theme } from '@mui/material/styles';


export const styles: { root: SxProps<Theme> } = {
  root: {
    fontSize: 20,
    backgroundColor: 'blue',
    color: theme => theme.palette.common.white,
  },
};
