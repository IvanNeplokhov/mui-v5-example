import React from 'react';
import { Chip } from '@mui/material';

import { styles } from './styles';


interface IProps {
  text: string,
}

export const ChipWayThree = ({ text }: IProps) => {
  return (
    <Chip label={text} sx={styles.root} />
  );
};
