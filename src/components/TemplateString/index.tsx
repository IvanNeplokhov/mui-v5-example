import React  from 'react';
import { Button, IconButton, Typography, typographyClasses, Box } from '@mui/material';
import { styled, css } from '@mui/material/styles';


const StyledSimpleTypography = styled(Typography)`
  font-size: 27px;
  color: white;
`

const StyledButtonWithOptions = styled(Button, {
  name: 'StyledButtonWithOptions', // ключ, используемый theme.components для указания styleOverrides и variants.
                                   // Также используется для создания файла label.
  label: 'styled-button', // суффикс таблицы стилей. Полезно для отладки.
  shouldForwardProp: (prop) => ( // Указывает какой prop следует пересылать в компонент.
    prop !== 'customPadding' && prop !== 'customFontSize'
  ),
  skipSx: false,
  // ... etc
})<{ customPadding?: string, customFontSize?: string }>`
  color: white;
  ${(props) => `padding: ${props.customPadding};`}
  ${(props) => `font-size: ${props.customFontSize};`}
`

const StyledTypographyWithProps = styled(Typography)<{ isBlue?: boolean }>`
  color: ${props => props.isBlue ? 'blue' : 'white' }
`

const StyledTypographyWithTheme = styled(Typography, {
  name: 'StyledTypographyWithTheme',
})`
  color: ${({ theme }) => theme.palette.common.white};
`
// color: ${(props) => props.theme.palette.common.white}; // Or like this

const StyledTypographyCopied = StyledTypographyWithProps.withComponent(Typography);

// Need to install @emotion/babel-config for use it
//
// const Child = styled('div')`
//   color: red;
// `
// const Parent = styled('div')`
//   ${Child} {
//     color: green;
//   }
// `


const StyledTypographyShouldForwardProp = styled(Typography, {
  // prop is PropertyKey (string | number | symbol)
  shouldForwardProp: (prop) => (
    prop !== 'customColor'
  )
})<{ customColor?: string }>`color: ${props => props.customColor}`;

const StyledDivWithBreakpoint = styled('div')`
  padding: 10px;
  color: white;

  ${({ theme }) => `${theme.breakpoints.up('lg')} {
    padding: 20px;
  }`}
`

const StyledDivWithShouldForwardProp = styled('div', {
  shouldForwardProp: (prop) => {
    return prop !== 'customPadding';
  }
})<{ customPadding?: string }>`
  color: white;
  
  ${({ customPadding }) => `padding: ${customPadding}`}
`

// CSS. Большая часть API, включая styled, является просто обёрткой над css.
// css - это тегированный литерал шаблона, который принимает стандартный
// css-текст с несколькими дополнительными функциями,
// такими как вложенность, псевдоселекторы и медиа-запросы.
// css возвращает строковое имя класса, которое может использоваться для любого элемента.
// Для блока стиля imageBase это будет что-то вроде css-imageBase-12345.
const StyledIconButton = styled(IconButton, {
  shouldForwardProp: (prop) => {
    return prop !== 'selfNegativeMargin';
  },
})<{ selfNegativeMargin?: boolean }>(
  ({ theme, selfNegativeMargin }) => css`
    color: white;
    padding: ${theme.spacing()};
    ${selfNegativeMargin && css`
      margin: -${theme.spacing()};
    `}
  `,
);

const StyledTypographyOverride = styled(Typography)`
  ${({ theme }) => `${[`& .${typographyClasses.root}`]} {
    color: ${theme.palette.common.white};
  }`}
`

// sx prop — это новый способ стилизации компонентов, ориентированный на быструю настройку.
// styled является функцией, а sx является prop для MUI component.
const SxBox = () => (
  <Box
    sx={{
      color: 'common.white',
      paddingBottom: theme => theme.spacing(), // 8px
      paddingTop: 1, // означает "theme.spacing(1)", не "1px"!
                     // в styled же поведение будет ожидаемое: 1px.
      mx: 1,         // margin по оси X. Как мы помним, такая запись = theme.spacing(1)
                     // mx (my) отсутствует в styled.
      fontSize: 34,  // но здесь 34px :)
    }}
  >
    11. I am Box styled with sx
  </Box>
);


export const TemplateString = () => {
  return (
    <>
      <StyledSimpleTypography>1. I am Typography with 27px font size</StyledSimpleTypography>
      <StyledButtonWithOptions variant="contained">2. I am Button</StyledButtonWithOptions>
      <StyledTypographyWithProps isBlue>3. I am Typography with style props</StyledTypographyWithProps>
      <StyledTypographyWithTheme>4. I am Typography with theme inside</StyledTypographyWithTheme>
      <StyledTypographyCopied isBlue>5. I am copied Typography from 3</StyledTypographyCopied>
      {/*<div>*/}
      {/*  <Parent>*/}
      {/*    <Child>Green because I am inside a Parent</Child>*/}
      {/*  </Parent>*/}
      {/*  <Child>Red because I am not inside a Parent</Child>*/}
      {/*</div>*/}
      <StyledTypographyShouldForwardProp customColor="white">6. I am Typography with shouldForwardProp()</StyledTypographyShouldForwardProp>
      <StyledDivWithBreakpoint>7. I am Div with breakpoints up lg</StyledDivWithBreakpoint>
      <StyledDivWithShouldForwardProp>8. I am Div with shouldForwardProp()</StyledDivWithShouldForwardProp>
      <StyledIconButton selfNegativeMargin>9. I am IconButton with css util</StyledIconButton>
      <StyledTypographyOverride>10. I am Typography with override</StyledTypographyOverride>
      <SxBox />
    </>
  );
};
