import { styled } from '@mui/material/styles';
import { Chip } from '@mui/material';
import { SerializedStyles } from '@emotion/react';


export interface IStyledChipProps {
  sColor?: string,
  sBackgroundColor?: string,
  css?: SerializedStyles,
}

export const SChip = styled(Chip, {
  shouldForwardProp: (prop: keyof IStyledChipProps) => (
    !['sColor', 'sBackgroundColor', 'css'].includes(prop)
  ),
})<IStyledChipProps>((props) => ({
  ...props.css,
  color: props.sColor,
  backgroundColor: props.sBackgroundColor,
}))
