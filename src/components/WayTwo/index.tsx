import React from 'react';
import { ChipProps } from '@mui/material';

import { IStyledChipProps, SChip } from './index.styled';
import { SerializedStyles } from '@emotion/react';


interface IProps extends Pick<ChipProps, 'sx' | 'variant'> {
  text: string,
  css?: SerializedStyles,
  color?: IStyledChipProps['sColor'],
  backgroundColor?: IStyledChipProps['sBackgroundColor'],
}

export const ChipWayTwo = ({
  sx,
  css,
  text,
  color,
  backgroundColor,
  variant = 'filled',
}: IProps) => {

  return (
    <SChip
      sx={sx}
      css={css}
      size="small"
      label={text}
      sColor={color}
      variant={variant}
      sBackgroundColor={backgroundColor}
    />
  );
};
