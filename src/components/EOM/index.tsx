import React, { FC } from 'react';
import { styled } from '@mui/material/styles';
import { Box, IconButton } from '@mui/material';
import { CounterArrowUpIcon, CounterArrowDownIcon } from 'static/icons';

import { getProcessedValue } from './utils';


const PREFIX = 'Counter';
const classes = {
  transparentIcon: `${PREFIX}-transparentIcon`,
  input: `${PREFIX}-input`,
  counterArrowsBtnsWrapper: `${PREFIX}-counterArrowsBtnsWrapper`,
  counterArrowBtn: `${PREFIX}-counterArrowBtn`,
};

const Root = styled(Box)(({ theme }) => ({
  width: 'inherit',
  position: 'relative',
  [`& .${classes.transparentIcon}`]: { color: theme.palette.common.white },
  [`& .${classes.input}`]: {
    width: 'inherit',
    height: '2rem',
    paddingLeft: theme.spacing(2),
    borderRadius: theme.shape.borderRadius,
    border: theme.border.default,
    '&::-webkit-inner-spin-button': { WebkitAppearance: 'none' },
  },
  [`& .${classes.counterArrowsBtnsWrapper}`]: {
    position: 'absolute',
    top: '6px',
    right: 0,
    display: 'flex',
    flexDirection: 'column',
    gap: theme.spacing(0.6),
    color: theme.palette.text.secondary,
    cursor: 'pointer',
    paddingRight: theme.spacing(1.2),
  },
  [`& .${classes.counterArrowBtn}`]: {
    padding: 0,
    color: theme.palette.text.secondary,
  },
}));


export interface IProps {
  readOnly?: boolean,
  value: string,
  onChange: (value: string) => void
  min?: number,
  max?: number,
  externalStyles: string
}

export const Counter: FC<IProps> = ({
  value,
  readOnly,
  onChange,
  externalStyles
}) => {
  const onChangeValue = (val: string, modifier: number) => {
    const adaptSaveValue = getProcessedValue(val, modifier, value);
    onChange(adaptSaveValue);
  };
  const adaptViewValue = value.replace('.', ',');
  return (
    <Root>
      <input
        className={clsx(externalStyles, classes.input)}
        disabled={readOnly}
        type="text"
        value={adaptViewValue}
        readOnly={readOnly}
        onChange={e => {
          onChangeValue(e.target.value, 0);
        }}
      />
      <Box className={classes.counterArrowsBtnsWrapper}>
        <IconButton
          className={classes.counterArrowBtn}
          onClick={() => onChangeValue(value, 1)}
        >
          <CounterArrowUpIcon />
        </IconButton>
        <IconButton
          className={classes.counterArrowBtn}
          onClick={() => onChangeValue(value, -1)}
        >
          <CounterArrowDownIcon />
        </IconButton>
      </Box>
    </Root>
  );
};