import React from 'react';
import { Chip } from '@mui/material';
import { styled } from '@mui/material/styles';


interface IProps {
  text: string,
  color?: IStyledProps['sColor'],
  variant?: 'filled' | 'outlined',
  backgroundColor?: IStyledProps['sBackgroundColor'],
}

export interface IStyledProps {
  sColor?: string,
  sBackgroundColor?: string,
}

const SChip = styled(Chip, {
  shouldForwardProp: (prop: keyof IStyledProps) => (
    prop !== 'sColor' && prop !== 'sBackgroundColor'
  ),
})<IStyledProps>((props) => ({
  color: props.sColor,
  backgroundColor: props.sBackgroundColor,
}))

export const ChipWayOne = ({
  text,
  color,
  backgroundColor,
  variant = 'filled',
}: IProps) => {

  return (
    <SChip
      size="small"
      label={text}
      sColor={color}
      variant={variant}
      sBackgroundColor={backgroundColor}
    />
  );
};
